Number.prototype.map = function(in_min, in_max, out_min, out_max) {
  //return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  return (this - in_min) / (in_max - in_min) * (out_max - out_min) + out_min;
};

export default class Point {
  constructor(width, height, activationFunc) {
    this.width = width;
    this.height = height;
    this.bias = 1;
    this.activationFunc = activationFunc;
    this._x = Math.random() * 2 - 1;
    this._y = Math.random() * 2 - 1;

    let canvasCoor = this.pointToCanvasCoordinate({ x: this._x, y: this._y });
    this.x = canvasCoor.x;
    this.y = canvasCoor.y;
    if (this._y > this.activationFunc(this._x)) {
      this.label = 1;
    } else {
      this.label = -1;
    }
  }

  pointToCanvasCoordinate = p => {
    return {
      x: p.x.map(-1, 1, 0, this.width),
      y: p.y.map(-1, 1, this.height, 0)
    };
  };

  getColor() {
    if (this.label === 1) {
      return "#795548";
    } else {
      return "#3F51B5";
    }
  }
}
