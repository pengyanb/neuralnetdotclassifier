import React, { Component } from "react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import { TextField, RaisedButton } from "material-ui";
import { Stage, Layer, Line, Circle } from "react-konva";
import async from "async";
import "./App.css";
import Perceptron from "./Perceptron";
import Point from "./Point";
class App extends Component {
  constructor(props) {
    super(props);
    let stageWidth = 500;
    let stageHeight = 500;
    this.perceptron = new Perceptron(3);
    let points = [];
    let pointsCount = 100;
    this.renderRefreshDelay = 50;

    this.state = {
      stageWidth: stageWidth,
      stageHeight: stageHeight,
      points: points,
      pointsCount: pointsCount,
      m: -0.6,
      b: 0.2, //y = m * x + b
      errorRate: 0,
      trainingInProgress: false,
      guessed: [],
      guessedLine: []
    };
    this.pt = new Point(stageWidth, stageHeight, this.activationFunc);
  }

  componentDidMount() {
    console.log(this.refs.stage);
  }

  activationFunc = x => {
    if (
      Number(this.state.m) > -1 &&
      Number(this.state.m) < 1 &&
      Number(this.state.b) > -1 &&
      Number(this.state.b) < 1
    ) {
      return Number(this.state.m) * x + Number(this.state.b);
    } else {
      return 0;
    }
  };

  startNeuralNetwork = () => {
    if (!this.state.trainingInProgress) {
      console.log("[startNeuralNetwork]");
      this.pt = new Point(
        this.state.stageWidth,
        this.state.stageHeight,
        this.activationFunc
      );
      let points = [];
      this.pointsCount = 100;
      this.perceptron = new Perceptron(3);
      for (let i = 0; i < this.state.pointsCount; i++) {
        points[i] = new Point(
          this.state.stageWidth,
          this.state.stageHeight,
          this.activationFunc
        );
      }
      let guessed = [];
      let errorCount = 0;
      for (let i = 0; i < points.length; i++) {
        let p = points[i];
        guessed[i] = this.perceptron.guess([p._x, p._y, p.bias]);
        if (guessed[i] !== p.label) {
          errorCount++;
        }
      }
      let startPoint = this.pt.pointToCanvasCoordinate({
        x: -1,
        y: this.perceptron.guessY(-1, 1)
      });
      let endPoint = this.pt.pointToCanvasCoordinate({
        x: 1,
        y: this.perceptron.guessY(1, 1)
      });
      let guessedLine = [startPoint.x, startPoint.y, endPoint.x, endPoint.y];
      this.setState({
        points: points,
        guessed: guessed,
        guessedLine: guessedLine,
        errorRate: (errorCount / this.state.pointsCount).toFixed(4)
      });
    }
  };
  autoTrainNeuralNetwork = () => {
    if (!this.state.trainingInProgress) {
      this.trainNeuralNetwork(() => {
        console.log("ErrRate: ", Number(this.state.errorRate));
        if (Number(this.state.errorRate) * 100 > 2) {
          this.autoTrainNeuralNetwork();
        }
      });
    }
  };
  trainNeuralNetwork = callback => {
    if (!this.state.trainingInProgress) {
      this.setState({ trainingInProgress: true });
      let points = this.state.points;
      for (let i = 0; i < points.length; i++) {
        let p = points[i];
        this.perceptron.train([p._x, p._y, p.bias], p.label);
      }

      async.eachSeries(
        points,
        (p, done) => {
          this.perceptron.train([p._x, p._y, p.bias], p.label);
          let guessed = [];
          let errorCount = 0;
          for (let i = 0; i < points.length; i++) {
            let p = points[i];
            guessed[i] = this.perceptron.guess([p._x, p._y, p.bias]);
            if (guessed[i] !== p.label) {
              errorCount++;
            }
          }
          let startPoint = this.pt.pointToCanvasCoordinate({
            x: -1,
            y: this.perceptron.guessY(-1, 1)
          });
          let endPoint = this.pt.pointToCanvasCoordinate({
            x: 1,
            y: this.perceptron.guessY(1, 1)
          });
          let guessedLine = [
            startPoint.x,
            startPoint.y,
            endPoint.x,
            endPoint.y
          ];
          this.setState(
            {
              guessed: guessed,
              guessedLine: guessedLine,
              errorRate: (errorCount / this.state.pointsCount).toFixed(4)
            },
            () => {
              setTimeout(() => {
                done();
              }, this.renderRefreshDelay);
            }
          );
        },
        err => {
          if (err) {
            console.log("[async.eachSeries err]: ", err);
          }
          this.setState({ trainingInProgress: false }, () => {
            if (typeof callback === "function") {
              callback();
            }
          });
        }
      );
    }
  };
  testNeuralNetwork = () => {
    if (!this.state.trainingInProgress) {
      let points = [];
      this.pointsCount = 100;
      for (let i = 0; i < this.state.pointsCount; i++) {
        points[i] = new Point(
          this.state.stageWidth,
          this.state.stageHeight,
          this.activationFunc
        );
      }
      let guessed = [];
      let errorCount = 0;
      for (let i = 0; i < points.length; i++) {
        let p = points[i];
        guessed[i] = this.perceptron.guess([p._x, p._y, p.bias]);
        if (guessed[i] !== p.label) {
          errorCount++;
        }
      }
      let startPoint = this.pt.pointToCanvasCoordinate({
        x: -1,
        y: this.perceptron.guessY(-1, 1)
      });
      let endPoint = this.pt.pointToCanvasCoordinate({
        x: 1,
        y: this.perceptron.guessY(1, 1)
      });
      let guessedLine = [startPoint.x, startPoint.y, endPoint.x, endPoint.y];
      this.setState({
        points: points,
        guessed: guessed,
        guessedLine: guessedLine,
        errorRate: (errorCount / this.state.pointsCount).toFixed(4)
      });
    }
  };
  renderPoints() {
    if (this.state.points.length > 0) {
      return this.state.points.map((p, index) => {
        let guessed =
          this.state.guessed.length > index ? this.state.guessed[index] : null;
        let target = p.label;
        let color = "#FFFFFF";
        if (guessed !== null) {
          color = target === guessed ? "green" : "red";
        }
        return (
          <Circle
            key={index}
            x={p.x}
            y={p.y}
            radius={6}
            fill={color}
            stroke={p.getColor()}
            strokeWidth={4}
          />
        );
      });
    } else {
      return null;
    }
  }
  render() {
    //console.log("[guessedLine]: ", this.state.guessedLine);

    let divideLineStart = this.pt.pointToCanvasCoordinate({
      x: -1,
      y: this.activationFunc(-1)
    });
    let divideLineEnd = this.pt.pointToCanvasCoordinate({
      x: 1,
      y: this.activationFunc(1)
    });
    let divideLine = [
      divideLineStart.x,
      divideLineStart.y,
      divideLineEnd.x,
      divideLineEnd.y
    ];
    return (
      <div className="App">
        <h1 className="App-title">{"Neural Network [Dot seperator]"}</h1>
        <MuiThemeProvider>
          <div>
            <TextField
              id="canvasWidth"
              floatingLabelText="Canvas width"
              value={this.state.stageWidth}
              style={{ margin: "5px" }}
              onChange={e => {
                let newValue = e.target.value;
                if (!isNaN(Number(newValue))) {
                  this.setState({ stageWidth: Number(newValue) }, () => {
                    this.startNeuralNetwork();
                  });
                }
              }}
            />
            <TextField
              id="canvasHeight"
              floatingLabelText="Canvas height"
              value={this.state.stageHeight}
              style={{ margin: "5px" }}
              onChange={e => {
                let newValue = e.target.value;
                if (!isNaN(Number(newValue))) {
                  this.setState({ stageHeight: Number(newValue) }, () => {
                    this.startNeuralNetwork();
                  });
                }
              }}
            />
            <TextField
              id="lineDividerM"
              floatingLabelText="m"
              value={this.state.m}
              style={{ margin: "5px" }}
              onChange={e => {
                let newValue = e.target.value;
                this.setState({ m: newValue }, () => {
                  newValue = Number(newValue);
                  if (newValue > -1 && newValue < 1) {
                    this.startNeuralNetwork();
                  }
                });
              }}
            />
            <TextField
              id="lineDividerB"
              floatingLabelText="b"
              value={this.state.b}
              style={{ margin: "5px" }}
              onChange={e => {
                let newValue = e.target.value;
                this.setState({ b: newValue }, () => {
                  newValue = Number(newValue);
                  if (newValue > -1 && newValue < 1) {
                    this.startNeuralNetwork();
                  }
                });
              }}
            />
            <h5>
              {"Line divider "}
              <strong>{"Y = m * X + b  "}</strong>
              <i>{"where [ -1 < m < 1 and -1 < b < 1] --- "}</i>
              <i>
                {"Error rate: " +
                  Number(this.state.errorRate).toFixed(4) * 100 +
                  "%"}
              </i>
            </h5>
            <br />
            <RaisedButton
              id="startButton"
              color="primary"
              style={{ margin: "2px" }}
              disabled={this.state.trainingInProgress}
              onClick={this.startNeuralNetwork}
            >
              Start
            </RaisedButton>
            <RaisedButton
              id="trainButton"
              color="primary"
              style={{ margin: "2px" }}
              disabled={this.state.trainingInProgress}
              onClick={this.trainNeuralNetwork}
            >
              Train
            </RaisedButton>
            <RaisedButton
              id="testButton"
              color="primary"
              style={{ margin: "2px" }}
              disabled={this.state.trainingInProgress}
              onClick={this.testNeuralNetwork}
            >
              Test
            </RaisedButton>
            <RaisedButton
              id="autoButton"
              color="primary"
              style={{ margin: "2px" }}
              disabled={this.state.trainingInProgress}
              onClick={this.autoTrainNeuralNetwork}
            >
              Auto
            </RaisedButton>
            <br />
          </div>
        </MuiThemeProvider>
        <div
          style={{
            position: "relative",
            width: this.state.stageWidth + "px",
            height: this.state.stageHeight + "px",
            left: "50%",
            marginLeft: "-" + this.state.stageWidth / 2 + "px",
            background: "#CFD8DC"
          }}
        >
          <Stage
            ref="stage"
            width={this.state.stageWidth}
            height={this.state.stageHeight}
          >
            <Layer>
              <Line points={divideLine} stroke="blue" strokeWidth={1} />
              {this.state.guessedLine.length > 0 ? (
                <Line
                  points={this.state.guessedLine}
                  stroke="red"
                  strokeWidth={1}
                />
              ) : null}
              {this.renderPoints()}
            </Layer>
          </Stage>
        </div>
      </div>
    );
  }
}

export default App;
