export default class Perceptron {
  constructor(weightLength) {
    //initialize weights
    this.learningRate = 0.01;
    this.weights = [];
    this.weightLength = weightLength;
    for (let i = 0; i < weightLength; i++) {
      this.weights[i] = Math.random() * 2 - 1; // -1 to 1
    }
  }

  guess(inputs) {
    let sum = 0;
    for (let i = 0; i < this.weights.length; i++) {
      sum += inputs[i] * this.weights[i];
    }
    let output = this.activationFunc(sum);
    return output;
  }

  guessY(x, bias) {
    // W0 * X + W1 * Y + W2 * B = 0
    // => Y = - (W0 * X + W2 * B) / W1
    //console.log("[weights]: ", x, this.weights);
    return (
      -1 * (this.weights[0] * x + this.weights[2] * bias) / this.weights[1]
    );
  }

  train(inputs, target) {
    let guessed = this.guess(inputs);
    let error = target - guessed;
    //Tune all the weight
    for (let i = 0; i < this.weights.length; i++) {
      this.weights[i] += error * inputs[i] * this.learningRate;
    }
  }

  activationFunc(value) {
    if (value > 0) {
      return 1;
    } else {
      return -1;
    }
  }
}
